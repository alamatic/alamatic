
from alamatic.types.base import *
from alamatic.types.integer import *
from alamatic.types.string import *
from alamatic.types.boolean import *
from alamatic.types.array import *
from alamatic.types.function import *
